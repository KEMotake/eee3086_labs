import threading
import time
import RPi.GPIO as GPIO
import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn

toggleBtn = 22
# Add a button to toggle the rate of sampling between 10s, 5s and 1s
def setup():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(toggleBtn,GPIO.IN,pull_up_down=GPIO.PUD_UP)

# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

# create the cs (chip select)
cs = digitalio.DigitalInOut(board.D5)

# create the mcp object
mcp = MCP.MCP3008(spi, cs)

#create an analog input channel on pin 0
#chan = AnalogIn(mcp, MCP.P0)
tempSensor = AnalogIn(mcp, MCP.P1)
LDR = AnalogIn(mcp, MCP.P2)

# the scale factor of MCP9700A (temperature sensor) is 10mV/°C with a 500 mV offset to allow for negative temperatures
# convert voltage to degree Celsius including the 500mV offset adjustment
#print('Runtime		Temp Reading		Temp			Light Reding')
#startTime = time.time()
# Fetch the sensor value five times
def fetch_sensor_vals(sensor_pin):
    for i in range(5):
        GPIO.input(sensor_pin)
        #time.sleep(2)
if __name__ == "__main__":
    setup()

    # Create a thread to call the function and pass "12" in as sensor pin
    x = threading.Thread(target=fetch_sensor_vals, args=(22,))

    print('Runtime          Temp Reading            Temp                    Light Reding')
    startTime = time.time()
    x.start()
    x.join()
    while True:
          global state
          state=0
          buttonState = GPIO.input(toggleBtn)
          if buttonState==False:
               state=+1
          if (state==0 and buttonState==True):
               print(int(time.time()-startTime),'		',tempSensor.value,'			'+str(round((tempSensor.voltage-0.5)*100,2)) + ' C'+'			',LDR.value)
               time.sleep(1)
          elif (state==1 and buttonState==False):
               print(int(time.time()-startTime),'		',tempSensor.value,'			'+str(round((tempSensor.voltage-0.5)*100,2)) + ' C'+'			',LDR.value)
               time.sleep(5)
          elif (state==2 and buttonState==True):
               print(int(time.time()-startTime),'		',tempSensor.value,'			'+str(round((tempSensor.voltage-0.5)*100,2)) + ' C'+'			',LDR.value)
               time.sleep(10)
    pass
