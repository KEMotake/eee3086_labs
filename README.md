# Raspberry Pi Practicals

## Overview

The Raspberry Pi  is a single board computer (SBC) and the computer system is on one PCB. The fundamental enabling component of the RPi is its system-on-chip (SOC) around which it is built. A SOC is an Integrated Circuit (IC) that
combines the various essential parts of a computer system into one chip – these are various
types of SOCs, not necessarily all implement a computer system, but the RPi uses the
Broadcom that integrates most of the pieces needed in a GPU-enabled computer.

The RPi like any other computer requires an operating system (OS) to make it useful.
An OS is a type of system software that manages the hardware and software resources of your computer to provide common services that computer programs need in order to run. The Raspbian OS is the one commonly used with the RPi, and
is the one we will use in our pracs. Raspbian is a version of the more commonly used (for PCs) Debian Linux OS; Raspbian
is provided and maintained by the Raspberry PI Foundation as the primary OS for their
family of RPis.

## Contributing to this repository
We would appreciate your input! We want to make contributing to this repository as easy and transparent as possible, whether it's:

- Setting up the raspberry Pi
- Debugging c language programs.
- Reporting any issues related to raspberry connections
- Discussing the current state of the microHAT
- Submitting a fix
- Proposing techniques

## Any contributions you make will be under the MIT Software License
In short, when you submit simulation changes, your submissions are understood to be under the same [MIT License](http://choosealicense.com/licenses/mit/) that covers the project. Feel free to contact the maintainers if that's a concern.
